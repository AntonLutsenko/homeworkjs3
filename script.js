// 1. функции необходимы для упрощения кода и уменьшения + если она написана 1н раз, то на нее можно ссылаться в последующем коде и она будет работать.
// 2. на места аргументов передаются необходимые значения. благодаря этому функцию можно использовать с любыми названиями переменных или констант

;
let firstUserNumber = +prompt('Enter 1st Number');
let secondUserNumber = +prompt('Enter 2nd Number');
let mathOperator = prompt('Enter math operator +, -, *, /')

function calculated (firstNum, secondNum, operator) {
    switch (operator) {
        case '+':
            return firstNum + secondNum;
        case '-':
            return firstNum - secondNum;
        case '*':
            return firstNum * secondNum;
        case '/':
            return firstNum / secondNum;
    }
}
console.log(calculated(firstUserNumber, secondUserNumber,mathOperator));

// let calculated = (firstNum, secondNum, operator) => {
//     switch (operator) {
//         case '+':
//             return firstNum + secondNum;
//         case '-':
//             return firstNum - secondNum;
//         case '*':
//             return firstNum * secondNum;
//         case '/':
//             return firstNum / secondNum;
//     }
// }
// console.log(calculated(firstUserNumber, secondUserNumber,mathOperator));